import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DisplayName("Running CalculatorTests")
class CalculatorTest {
    Calculator calc;

    @BeforeEach
    void setUp() {
        calc = new Calculator();
    }

    @Nested
    class AddTest {

        @Test
        @Order(1)
        @DisplayName("testing addmethod for +")
        void testAddition() {
            assertEquals(4.0, calc.addition(2.0, 2.0), () -> "should add two numbers");
        }

        @Test
        @Order(2)
        @DisplayName("testing addmethod for -")
        void testAddition2() {
            assertEquals(-5, calc.addition(-3, -2), () -> "should add two numbers");
        }

    }

    @Test
    void testSubraction() {
        assertEquals(2, calc.subtraction(4.0, 2.0));
        assertEquals((-5), calc.subtraction(5.0, 10.0));
    }

    @ParameterizedTest
    @ValueSource(ints = {-3, -1, 5, 101})
    void testIsOdd(int numbers) {
        assertTrue(calc.isOdd(numbers));
    }

    @Test
    void testCalculateCircleArea() {
        assertEquals(6.16, calc.calculateCircleArea(2.8));
        assertThrows(IllegalArgumentException.class, () -> {calc.calculateCircleArea(-3.4);});
    }

    @Test
    @DisplayName("Exception is thrown when divided by 0")
    void testArithmeticException() {
        assertThrows(ArithmeticException.class, () -> {calc.division(10, 0);});
    }

    @Test
    void testRemoveItemFromList() {
        int[] list = {1,2,3,4,5};
        int[] list2 = {1};
        assertEquals(4, (calc.removeLastItemFromList(list)).length, "List-size should be 4");
        assertThrows(IndexOutOfBoundsException.class, () -> calc.removeLastItemFromList(list2) ,"should throw IndexoutofBoundsException" );
        assertNotSame(list, calc.removeLastItemFromList(list), "Objectreference should not be same");
    }

    @AfterEach
    void tearDown(){
        calc = null;
    }

}