

public class Calculator {

    public Calculator(){
    }

    public double addition(double a, double b){
        return a+b;
    }

    public double subtraction(double a, double b){
        return a-b;
    }

    public double division(int a, int b){
        return a/b;
    }

    public double calculateCircleArea(double diameter){
        double radius, a = 0;
            radius = calculateRadius(diameter);
            a = Math.PI * radius * radius;
        return Math.round(a * 100) / 100D;

    }

    private double calculateRadius(double diameter){
        if(diameter <= 0){
            throw new IllegalArgumentException();}
        return diameter/2;
    }

    public boolean isOdd(int number){
        return number % 2 != 0;
    }

    public int[] removeLastItemFromList(int[] list){
        if(list.length<=1)
            throw new IndexOutOfBoundsException();
        int[] newList = new int[list.length-1];

        for(int i=0; i<list.length-1; i++) {
            newList[i]=list[i];
        }
        return newList;
    }
}
